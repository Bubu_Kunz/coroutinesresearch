package com.yzubritskiy.coroutinesreport

import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.View
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.newSingleThreadContext
import java.util.*
import java.util.concurrent.Executors
import java.util.concurrent.ThreadLocalRandom


class SuspendingVsBloking {

    companion object {
        val TAG = "SuspendingVsBloking2"


        fun prepareSteps(): ArrayList<Float> {
            val alphas = ArrayList<Float>()
            val alphaIteration = 0.01f
            var alpha = 1f
            while (alpha >= 0.5f) {
                alphas.add(alpha)
                alpha -= alphaIteration
            }
            alphas.addAll(alphas.reversed())
            return alphas
        }

        fun doRx(view1: View, view2: View, view3: View) {
            val preparedSteps = prepareSteps()
            processWithRx(view1, preparedSteps)
            processWithRx(view2, preparedSteps)
            processWithRx(view3, preparedSteps)
        }

        val singleThreadScheduler = Schedulers.single()

        fun processWithRx(view: View, preparedSteps: ArrayList<Float>) {
            val width = view.width
            val height = view.height
            Observable.just(preparedSteps)
                    .subscribeOn(singleThreadScheduler)
                    .flatMapIterable { alphas -> alphas }
                    .doOnNext { blockingFuncWithDelay() }
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ step ->
                        processStep(view, width, step, height)
                    },
                            { error -> Log.e(TAG, error.message, error) })

        }

        private fun blockingFuncWithDelay() {
            Thread.sleep(ThreadLocalRandom.current().nextLong(10, 100))
        }

        suspend private fun suspendFuncWithDelay() {
            delay(ThreadLocalRandom.current().nextLong(10, 100))
        }

        fun doExecutor(view1: View, view2: View, view3: View) {
            val preparedSteps = prepareSteps()
            processWithExecutor(view1, preparedSteps)
            processWithExecutor(view2, preparedSteps)
            processWithExecutor(view3, preparedSteps)
        }

        val singleThreadExecutor = Executors.newSingleThreadExecutor()
        private val handler = Handler(Looper.getMainLooper())

        fun processWithExecutor(view: View, preparedSteps: ArrayList<Float>) {
            val width = view.width
            val height = view.height
            for (step in preparedSteps) {
                singleThreadExecutor.execute {
                    blockingFuncWithDelay()
                    handler.post {
                        processStep(view, width, step, height)
                    }
                }
            }
        }

        fun doCoroutine(view1: View, view2: View, view3: View) {
            val preparedSteps = prepareSteps()
            processWithCoroutins(view1, preparedSteps)
            processWithCoroutins(view2, preparedSteps)
            processWithCoroutins(view3, preparedSteps)
        }

        val singleThreadContext = newSingleThreadContext("alpha")

        fun processWithCoroutins(view: View, preparedSteps: ArrayList<Float>) {
            val width = view.width
            val height = view.height
            launch(singleThreadContext) {
                for (step in preparedSteps) {
                    suspendFuncWithDelay()
                    launch(UI) {
                        processStep(view, width, step, height)
                    }
                }
            }
        }

        private fun processStep(view: View, width: Int, step: Float, height: Int) {
            val params = view.layoutParams;
            params.width = (width * step).toInt()
            params.height = (height * step).toInt()
            view.layoutParams = params
        }
    }
}
