package com.yzubritskiy.coroutinesreport

import android.util.Log
import com.yzubritskiy.coroutinesreport.data.PokemonTCGRepository
import com.yzubritskiy.coroutinesreport.data.models.Cards
import io.reactivex.functions.Consumer
import kotlinx.coroutines.experimental.IO
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.launch

class RetrofitIddlingExample {

    companion object {
        //Example of network call using retrofit and rxadapter
        fun rxExample() {
            val repo = PokemonTCGRepository()
            Log.d("TAG", "start: ")
            repo.getCardsRx(Consumer { c ->
                Log.d("TAG", "cards: "+ c?.cards)
            })
        }

        //Example of bocking network call using Retrofit Call<T>
        //and coroutines IO-coroutine context
        fun blockingIOExample() {
            val repo = PokemonTCGRepository()
            Log.d("TAG", "start: ")
            launch(UI) {
                var c: Cards? = null
                async(IO) { c = repo.getCards() }.await()
                Log.d("TAG", "cards: "+ c?.cards)
            }
        }

        fun asyncExample() {
            val repo = PokemonTCGRepository()
            Log.d("TAG", "start: ")
            launch {
                var c: Cards? = repo.getCardsDeffered()
                Log.d("TAG", "cards: "+ c?.cards)
            }
        }
        fun asyncContinuationExample() {
            val repo = PokemonTCGRepository()
            Log.d("TAG", "start: ")
            launch {
                var c: Cards? = repo.getCardsContinuation()
                Log.d("TAG", "cards: "+ c?.cards)
            }
        }
    }
}