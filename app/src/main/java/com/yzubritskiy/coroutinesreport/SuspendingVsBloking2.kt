package com.yzubritskiy.coroutinesreport

import android.util.Log
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.launch
import java.util.concurrent.Callable
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit


class SuspendingVsBloking2 {

    companion object {
        val TAG = "SuspendingVsBloking2"


        fun doCoroutine() {
            launch(UI) {
                doSimpleCoroutineWork1()
                doHeavyCoroutineWork()
                doSimpleCoroutineWork2()
            }
        }

        suspend fun doSimpleCoroutineWork1() {
            delay(100)
            Log.d(TAG, "doSimpleCoroutineWork1")
        }

        suspend fun doSimpleCoroutineWork2() {
            delay(200)
            Log.d(TAG, "doSimpleCoroutineWork2")
        }

        suspend fun doHeavyCoroutineWork() {
            delay(2000)
            Log.d(TAG, "doHeavyCoroutineWork")
        }

        fun doJava() {
            doSimpleExecutorWork1()
            doHeawyExecutorWork()
            doSimpleExecutorWork2()
        }

        val executor = Executors.newFixedThreadPool(3)

        fun doSimpleExecutorWork1() {
            val message = executor.submit(Callable {
                Thread.sleep(100)
                "doSimpleExecutorWork1"
            }).get()
            Log.d(TAG, message)
        }

        fun doSimpleExecutorWork2() {
            val message = executor.submit(Callable {
                Thread.sleep(200)
                "doSimpleExecutorWork2"
            }).get()
            Log.d(TAG, message)
        }

        fun doHeawyExecutorWork() {
            val message = executor.submit(Callable {
                Thread.sleep(2000)
                "doHeawyExecutorWork"
            }).get()
            Log.d(TAG, message)
        }

        fun doRx() {
            doSimpleRxWork1()
            doHeavyRxWork()
            doSimpleRxWork2()
        }

        fun doSimpleRxWork1() {
            Observable.just("processWithRx")
                    .delay(100, TimeUnit.MILLISECONDS)
                    .subscribeOn(Schedulers.computation())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ message ->
                        Log.d(TAG, message)
                    })

        }

        fun doSimpleRxWork2() {
            Observable.just("doSimpleRxWork2")
                    .delay(200, TimeUnit.MILLISECONDS)
                    .subscribeOn(Schedulers.computation())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ message ->
                        Log.d(TAG, message)
                    })

        }

        fun doHeavyRxWork() {
            Observable.just("doHeavyRxWork")
                    .delay(2000, TimeUnit.MILLISECONDS)
                    .subscribeOn(Schedulers.computation())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ message ->
                        Log.d(TAG, message)
                    })

        }
    }
}
