package com.yzubritskiy.coroutinesreport

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import com.yzubritskiy.coroutinesreport.data.PokemonTCGRepository
import com.yzubritskiy.coroutinesreport.data.models.Card
import kotlinx.android.synthetic.main.activity_card_list.*
import kotlinx.coroutines.experimental.IO
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.withContext

class CardListActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_card_list)
        val adapter = CardsListAdapter()
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this)
        val repo = PokemonTCGRepository()
        launch(UI) {
            val cards = withContext(IO) { repo.getCardsDeffered() }
            Log.d("TAG", "cards->" + cards?.cards?.size + ", thread->" + Thread.currentThread().name)
            adapter.setCards(cards?.cards as ArrayList<Card>)
        }
    }
}
