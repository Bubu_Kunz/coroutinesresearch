package com.yzubritskiy.coroutinesreport

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.yzubritskiy.coroutinesreport.data.models.Card
import kotlinx.android.synthetic.main.item_card.view.*

class CardsListAdapter : RecyclerView.Adapter<CardsListAdapter.CardViewHolder>() {
    private var items = ArrayList<Card>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CardViewHolder {
        return CardViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_card, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: CardViewHolder, position: Int) {
        holder.bind(items[position])
    }

    fun setCards(cards: List<Card>) {
        items.clear()
        items.addAll(cards)
        notifyDataSetChanged()
    }


    class CardViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        fun bind(card: Card) {
            itemView.cardNameTextView.text = card.name
        }
    }
}