package com.yzubritskiy.coroutinesreport.data;

import com.yzubritskiy.coroutinesreport.data.models.Cards;

import io.reactivex.Single;
import kotlinx.coroutines.experimental.Deferred;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.GET;

public interface CardsService {

    @GET("/v1//cards?cards?page=2")
    Call<Cards> getCards();

    @GET("/v1//cards?cards?page=2")
    Single<Cards> getCardsRx();

    @GET("/v1//cards?cards?page=2")
    Deferred<Response<Cards>> getCardsDeferred();
}
