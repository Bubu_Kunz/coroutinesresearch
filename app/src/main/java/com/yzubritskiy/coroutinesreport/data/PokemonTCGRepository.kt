package com.yzubritskiy.coroutinesreport.data

import android.util.Log
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.experimental.CoroutineCallAdapterFactory
import com.yzubritskiy.coroutinesreport.data.models.Cards
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.experimental.suspendCancellableCoroutine
import retrofit2.*
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import kotlin.coroutines.experimental.suspendCoroutine


class PokemonTCGRepository{

    companion object {
        fun createCardsService(): CardsService {

            val retrofit = Retrofit.Builder()
                    .addConverterFactory(
                            GsonConverterFactory.create())
                    .baseUrl("https://api.pokemontcg.io/")
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addCallAdapterFactory(CoroutineCallAdapterFactory())
                    .build()

            return retrofit.create(CardsService::class.java)
        }
    }

    fun getCards(): Cards {
        val service = createCardsService()
        val response = service.cards.execute()
        return response.body()!!
    }

    suspend fun getCardsContinuation() : Cards {
        val service = createCardsService()
        val cards = service.cards.await()
        return cards!!
    }

    suspend fun getCardsDeffered() : Cards? {
        val service = createCardsService()
        val cards = service.cardsDeferred.await().body()
        return cards
    }

    suspend fun getCardsSuspending() : Cards {
        val service = createCardsService()
        return getResult(service.cards)
    }


    fun getCardsRx(callback: Consumer<Cards>) : Unit {
        Log.d("TAG", "getCardsRx: ")
        val service = createCardsService()
        service.cardsRx
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(callback)
    }


    //Will suspend coroutine and will wait until resume or resumeWithException call
    //Example of non-extension function
    suspend fun <T : Any> getResult(call: Call<T>): T = suspendCoroutine {
        call.enqueue(object : Callback<T> {

            override fun onFailure(call: Call<T>, error: Throwable) = it.resumeWithException(error)

            override fun onResponse(call: Call<T>, response: Response<T>) {
                response.body()?.run { it.resume(response.body()!!) }
                response.errorBody()?.run { it.resumeWithException(HttpException(response)) }
            }
        })
    }

    //Will suspend coroutine and will wait until resume or resumeWithException call
    //Example of extension-function
    public suspend fun <T : Any> Call<T>.await(): T {
        return suspendCancellableCoroutine { continuation ->
            enqueue(object : Callback<T> {
                override fun onResponse(call: Call<T>?, response: Response<T?>) {
                    if (response.isSuccessful) {
                        val body = response.body()
                        if (body == null) {
                            continuation.resumeWithException(
                                    NullPointerException("Response body is null: $response")
                            )
                        } else {
                            continuation.resume(body)
                        }
                    } else {
                        continuation.resumeWithException(HttpException(response))
                    }
                }

                override fun onFailure(call: Call<T>, t: Throwable) {
                    // Don't bother with resuming the continuation if it is already cancelled.
                    if (continuation.isCancelled) return
                    continuation.resumeWithException(t)
                }
            })

        }
    }
}