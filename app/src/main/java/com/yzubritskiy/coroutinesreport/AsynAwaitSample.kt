package com.yzubritskiy.coroutinesreport

import android.util.Log
import io.reactivex.Observable
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.experimental.Deferred
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.launch
import java.util.concurrent.TimeUnit

class AsynAwaitSample {
    companion object {
        fun doRxAsync() {
            val obString1 = Observable.just("iTomych").delay(1, TimeUnit.SECONDS)
            val obString2 = Observable.just("Studio").delay(2, TimeUnit.SECONDS)
            Observable.zip(obString1, obString2,
                    BiFunction<String, String, String> { s1, s2 ->
                        return@BiFunction "$s1 $s2"
                    })
                    .subscribeOn(Schedulers.computation())
                    .subscribe { resultString ->
                        Log.d("TAG", "resultString = $resultString")
                    }
        }

        fun doCoroutineAsync() {
            launch {
                val defString1 = async { getString1() }
                val defString2 = async { getString2() }
                val resultString = defString1.await() + " " + defString2.await()
                Log.d("TAG", "resultString = $resultString")
            }
        }

        fun doCoroutineAsyncShort() {
            launch {
                val resultString = getStringDeffer1().await() + " " + getStringDeffer2().await()
                Log.d("TAG", "resultString = $resultString")
            }
        }

        suspend fun exceptionExample() {
            delay(500)
            makeError()
        }

        private fun makeError() {
            throw IllegalStateException("Wrong state")
        }

        suspend fun getString1(): String {
            delay(1000)
            return "iTomych"
        }

        suspend fun getStringDeffer1(): Deferred<String> = async {
            delay(1000)
            return@async "iTomych"
        }

        suspend fun getString2(): String {
            delay(2000)
            return "Studio"
        }

        suspend fun getStringDeffer2(): Deferred<String> = async {
            delay(2000)
            return@async "Studio"
        }


    }
}