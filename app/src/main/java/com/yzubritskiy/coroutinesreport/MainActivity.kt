package com.yzubritskiy.coroutinesreport

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Toast
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.experimental.*
import kotlinx.coroutines.experimental.android.UI
import java.util.*
import java.util.concurrent.Callable
import java.util.concurrent.Executors
import kotlin.coroutines.experimental.suspendCoroutine

class MainActivity : AppCompatActivity() {
    val executorService = Executors.newSingleThreadExecutor()

    companion object {
        val TAG = "TAG"
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        rxAllocation()
    }

    suspend fun doWorkAndShowMessage() {
        delay(2000)
        Toast.makeText(this, "iTomych Studio", Toast.LENGTH_LONG).show()
    }


    private suspend fun coroutinesAllocation(d: Long): String {
        delay(d)
        delay(d)
        delay(d)
        return suspendCoroutine { cont ->
            cont.resume("Coroutine done")
        }
    }

    private fun rxAllocation() {
        Observable.just(1000L)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext { i -> Thread.sleep(i) }
                .doOnNext { i -> Thread.sleep(i) }
                .doOnNext { i -> Thread.sleep(i) }
                .map { "Rx done" }
                .subscribeOn(Schedulers.computation())
                .subscribe { s -> textView2.text = s }
    }
}
