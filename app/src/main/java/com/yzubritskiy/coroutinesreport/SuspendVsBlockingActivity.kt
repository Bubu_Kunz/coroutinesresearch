package com.yzubritskiy.coroutinesreport

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_suspend_vs_blocking.*

class SuspendVsBlockingActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_suspend_vs_blocking)
        doCoroutineBtn.setOnClickListener {
            SuspendingVsBloking.doCoroutine(view1, view2, view3)
        }
        doRxBtn.setOnClickListener {
            SuspendingVsBloking.doRx(view1, view2, view3)
        }
        doExecutorBtn.setOnClickListener {
            SuspendingVsBloking.doExecutor(view1, view2, view3)
        }
    }


}
