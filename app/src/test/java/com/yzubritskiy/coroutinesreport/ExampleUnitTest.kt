package com.yzubritskiy.coroutinesreport

import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Test

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() = runBlocking {
        val value1 = addTwoInts(2, 2)
        val value2 = addTwoInts(2, value1)
        assertEquals(4, value1)
        assertEquals(6, value2)
    }

    suspend fun addTwoInts(num1: Int, num2: Int): Int {
        delay(2000)
        return num1 + num2
    }

}
